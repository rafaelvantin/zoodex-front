import { StatusBar } from "react-native";

// StatusBar.setBackgroundColor('#27C553');
StatusBar.setBarStyle("dark-content");
StatusBar.setHidden(true);
